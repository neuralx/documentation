# Documentation

Documentation for my projects / Learning new languages / Frameworks

Languages
1. Python       : (Default)
2. Rust         : (Low-level | Game Dev)
3. C | C++      : (Low-level | Fundamentals)
4. Haskell      : (Functional Programming)
5. Web Dev      : (Everything front-end related | some backend)

Frameworks:
1. Django

Linux Sysadmin | Engineer
